package apis

import (
	"apis/controllers"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

func Start() {

	router := mux.NewRouter()
	router.HandleFunc("/hc", GetHealthCheckController).Methods("GET")
	router.HandleFunc("/postImage", controllers.PostImage).Methods("POST")

	// Output startup message
	// Output startup message
	fmt.Println("\n-----------------------------------------")
	fmt.Println("       Server is running...")
	fmt.Println("----------------------------------")
	fmt.Println("\nLogs start here...")
	fmt.Println("-->")
	fmt.Println("")

	err := http.ListenAndServe(":80", router)
	if err != nil {
		log.Fatal("Error listening on port 80")
		return
	}
}

// GetHealthCheckController
func GetHealthCheckController(w http.ResponseWriter, r *http.Request) {
	// Success response
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("ok"))
	return
}
