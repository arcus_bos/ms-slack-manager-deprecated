package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"time"
)

// todo: for security change these into env variables
const slackToken = "xoxb-41212448566-481352056966-SkXFteRndjYtf2rGwFuHQ5s0"
const slackChannel = "CE3611EC8"

type SlackPayload struct {
	Text      string `json:"text, omitempty"`
	ImagePath string `json:"image_path, omitempty"`
	Comment   int    `json:"comment, omitempty"`
}

func PostImage(w http.ResponseWriter, r *http.Request) {

	defer r.Body.Close()
	var sp SlackPayload
	_ = json.NewDecoder(r.Body).Decode(&sp)

	target_url := generateURLwithParameters(sp)
	postFile(sp.ImagePath, target_url)
}

func addAndreasComments(comment int) string {

	andreasNegComments := []string{
		"Stop hanging out in the kitchen and get back to work!",
		"Got plans this weekend? Not anymore!",
		"If we can't support this, we can't get customers!",
		"Nothing is working! Fix it!",
		"Anther issue! This is completely unnaceptable!",
		"As long as nobody EVER see this, then we are OK.",
		"Somebody verify this!",
		"Everything is wrong. Just wrong...",
		"Canon will hate this.",
		"I hate to be the bad guy, but we're F#&$^@ if any customers see this.",
		"I'll order pizza. Looks like it's going to be a long night for you.",
		"My nerf gun is pointed right at you :gun:",
		"Web and video :thumbsdown:",
		"Din jävla idiot! För fan i helvete! Excuse my swedish.",
	}

	if comment < 0 || comment > len(andreasNegComments) {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		comment = r1.Intn(len(andreasNegComments))
	}

	return andreasNegComments[comment]
}

// x-www-form-urlencoded parameters
func generateURLwithParameters(payload SlackPayload) string {
	u, err := url.Parse("https://slack.com/api/files.upload")
	if err != nil {
		fmt.Println(err)
	}

	andreasComment := addAndreasComments(payload.Comment)

	q := u.Query()
	q.Set("token", slackToken)
	q.Set("channels", slackChannel)
	q.Set("content", "content")
	//q.Set("filename", "filename")
	q.Set("color", "#36a64f")
	q.Set("initial_comment", andreasComment+"\n"+payload.Text)
	q.Set("title", "Error Screenshot")
	u.RawQuery = q.Encode()

	fmt.Println("\n\n", u.String())
	return u.String()
}

// Multipart/form-data to handle the file
func postFile(filename string, targetUrl string) error {
	bodyBuf := &bytes.Buffer{}
	bodyWriter := multipart.NewWriter(bodyBuf)

	// this step is very important
	fileWriter, err := bodyWriter.CreateFormFile("file", filename)
	if err != nil {
		fmt.Println("error writing to buffer")
		return err
	}

	// open file handle
	fh, err := os.Open(filename)
	if err != nil {
		fmt.Println("error opening file")
		return err
	}
	defer fh.Close()

	//iocopy
	_, err = io.Copy(fileWriter, fh)
	if err != nil {
		return err
	}

	contentType := bodyWriter.FormDataContentType()
	bodyWriter.Close()

	resp, err := http.Post(targetUrl, contentType, bodyBuf)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	resp_body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(resp.Status)
	fmt.Println(string(resp_body))
	return nil
}
