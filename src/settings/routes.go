package settings

import (
	"apis"
)

/*
	Register routes mapping to each http handler
*/
func RegisterRoutes() {
	// Register each route here and map to handlers under controllers
	apis.Start()
}
