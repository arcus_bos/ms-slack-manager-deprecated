FROM alpine

LABEL maintainer="Baiyu Liu bli@arcusnext.com"

# Support HTTPS call to other services
ADD buildenv/ca-certificates.crt /etc/ssl/certs/

COPY bin/server /server
# Expose 80 port to proxy server
EXPOSE 80

ENTRYPOINT ["/server"]