{{/* Parse affinity, or use predefined affinity types */}}
{{- define "process.affinity" }}
    affinity:
  {{- if .Values.definedAffinity }}
    {{- if eq .Values.definedAffinity "highcpu" }}
      {{- with .Values.highCpuAffinity }}
{{ toYaml . | indent 6 }}
      {{- end }}
    {{- else if eq .Values.definedAffinity "highmem" }}
      {{- with .Values.highMemAffinity }}
{{ toYaml . | indent 6 }}
      {{- end }}
    {{- else if eq .Values.definedAffinity "gpu" }}
      {{- with .Values.gpuAffinity }}
{{ toYaml . | indent 6 }}
      {{- end }}
    {{- end }}
    {{- else if .Values.affinity }}
      {{- with .Values.affinity }}
{{ toYaml . | indent 6 }}
      {{- end }}
    {{- end }}
{{- end }}
{{/* End of process.affinity */}}

{{/* Render Configmaps, pulls in any special branches */}}
{{- define "render.configmaps" }}
  {{- $configmap := .Values.k8s.application -}}
    {{- if .Values.project }}
      {{- if eq .Values.project.environment "production" }}
        {{- range $key, $val := .Values.productionConfigmap }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $key }}
      name: {{ $configmap }}
        {{- end }}
      {{- end }} {{/* Production Configmaps */}}
      {{- if (eq .Values.project.environment "integration") }}
        {{- range $key, $val := .Values.integrationConfigmap }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $key }}
      name: {{ $configmap }}
        {{- end }}
      {{- end }} {{/* Integration Configmaps */}}
      {{- if eq .Values.project.environment "development" }}
        {{- range $key, $val := .Values.developmentConfigmap }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $key }}
      name: {{ $configmap }}
        {{- end }}
      {{- end }} {{/* Development configmaps */}}
      {{- if eq .Values.project.environment "preview" }}
        {{- range $key, $val := .Values.previewConfigmap }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $key }}
      name: {{ $configmap }}
        {{- end }}
      {{- end }} {{/* Preview configmaps */}}
    {{- end }}
  {{- if .Values.externalConfigmap }}
    {{- range $key, $val := .Values.externalConfigmap }}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $val.key }}
      name: {{ $val.configmap }}
    {{- end }}
  {{- end }}
  {{- if .Values.sslConfigmap }}
- name: NO_SSL
  value: "YES"
 {{- end }}
  {{- if .Values.explictConfigmap }}
    {{- range $key, $val := .Values.explictConfigmap }}
- name: {{ $key }}
  value: {{ $val }}
    {{- end }}
  {{- end }}
  {{- $configmap := .Values.k8s.application -}}
  {{- range $key, $val := .Values.configmap }} {{/* Generic configmaps */}}
- name: {{ $key }}
  valueFrom:
    configMapKeyRef:
      key: {{ $key }}
      name: {{ $configmap }}
  {{- end }} {{/* End of configmaps */}}
{{- end }}
{{/* End of render.configmaps */}}

{{/* Render.secrets */}}
{{- define "render.secrets" }}
  {{- if .Values.secrets }}
    {{- range $key, $val := .Values.secrets }}
- name: {{ $key }}
  valueFrom:
    secretKeyRef:
      key: {{ $val.key }}
      name: {{ $val.secretMap }}
    {{- end }}
  {{- end }}
{{- end }}
{{/* End of render.secrets */}}

{{/* Template for cloudsqlproxy */}}
{{- define "cloudsqlproxy" }}
      - args:
  {{- if .Values.project }}
    {{- if eq .Values.project.environment "development" }}
      {{- if .Values.cloudsqlproxy.development }}
        {{- $server := .Values.cloudsqlproxy.development.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- else }}
        {{- $server := .Values.cloudsqlproxy.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- end }}
    {{- end }}
    {{- if eq .Values.project.environment "integration" }}
      {{- if (.Values.cloudsqlproxy.integration) }}
        {{- $server := .Values.cloudsqlproxy.integration.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- else }}
        {{- $server := .Values.cloudsqlproxy.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- end }}
    {{- end }}
    {{- if eq .Values.project.environment "preview" }}
      {{- if (.Values.cloudsqlproxy.preview) }}
        {{- $server := .Values.cloudsqlproxy.preview.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- else }}
        {{- $server := .Values.cloudsqlproxy.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- end }}
    {{- end }}
    {{- if eq .Values.project.environment "production" }}
      {{- if (.Values.cloudsqlproxy.production) }}
        {{ $server := .Values.cloudsqlproxy.production.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- else }}
        {{ $server := .Values.cloudsqlproxy.serverName }}
        - -instances={{ .Values.project.projectId }}:{{ .Values.project.region | default "" }}:{{ $server }}=tcp:{{ .Values.cloudsqlproxy.port }}
      {{- end }}
    {{- end }}
        - -credential_file=/secrets/cloudsql/credentials.json
        command:
        - /cloud_sql_proxy
        env:
        image: gcr.io/squad-super-heroes-dev/mysql-proxy/gce-proxy:1.09
        imagePullPolicy: IfNotPresent
        name: squad-super-heroes-dev-mysql-proxy-gce-proxy
        ports:
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /secrets/cloudsql
          name: serviceaccount-sa-cloud-sql-proxy
  {{- else }}
    # Missing the project Values.yaml File
  {{- end }}
{{- end }}

{{/* Generate the configmap file */}}
{{- define "generateConfigmap" }}
  {{- range $key, $val  := .Values.configmap }}
{{ $key }}: {{ $val | quote }}
  {{- end }}
  {{- if .Values.project }}
    {{- if eq .Values.project.environment "production" }}
      {{- range $key, $val := .Values.productionConfigmap }}
{{ $key }}: {{$val | quote }}
      {{- end }}
    {{- else if eq .Values.project.environment "integration" }}
      {{- range $key, $val := .Values.integrationConfigmap }}
{{ $key }}: {{$val | quote }}
      {{- end }}
    {{- else if eq .Values.project.environment "preview" }}
      {{- range $key, $val := .Values.previewConfigmap }}
{{ $key }}: {{$val | quote }}
      {{- end }}
    {{- else if eq .Values.project.environment "development" }}
      {{- range $key, $val := .Values.developmentConfigmap }}
{{ $key }}: {{$val | quote }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}

{{/* Generate the goose connection string */}}
{{- define "generateGoose" }}
{{- if .Values.project }}
  {{- if eq .Values.project.environment "development" }}
    {{- $DB_USER := .Values.developmentConfigmap.DB_USER }}
    {{- $DB_NAME := .Values.developmentConfigmap.DB_NAME }}
    {{- $DB_HOST := .Values.developmentConfigmap.DB_HOST }}
    {{- $DB_PORT := .Values.developmentConfigmap.DB_PORT }}
    {{- $DB_PASSWORD := .Values.developmentConfigmap.DB_PASSWORD }} {{/* Postgres only for now */}}
        - {{ .Values.cloudsqlproxy.databaseType }}
        - host=127.0.0.1 port={{ $DB_PORT }} user={{ $DB_USER }} password=$DB_PASSWORD sslmode=disable dbname={{ $DB_NAME }}
        - up
  {{- end }}
  {{- if eq .Values.project.environment "integration" }}
    {{- $DB_USER := .Values.integrationConfigmap.DB_USER }}
    {{- $DB_NAME := .Values.integrationConfigmap.DB_NAME }}
    {{- $DB_HOST := .Values.integrationConfigmap.DB_HOST }}
    {{- $DB_PORT := .Values.integrationConfigmap.DB_PORT }}
    {{- $DB_PASSWORD := .Values.integrationConfigmap.DB_PASSWORD }}
        - {{ .Values.cloudsqlproxy.databaseType }}
        - host=127.0.0.1 port={{ $DB_PORT }} user={{ $DB_USER }} password=$DB_PASSWORD sslmode=disable dbname={{ $DB_NAME }}
        - up
  {{- end }}
  {{- if eq .Values.project.environment "preview" }}
    {{- $DB_USER := .Values.previewConfigmap.DB_USER }}
    {{- $DB_NAME := .Values.previewConfigmap.DB_NAME }}
    {{- $DB_HOST := .Values.previewConfigmap.DB_HOST }}
    {{- $DB_PORT := .Values.previewConfigmap.DB_PORT }}
    {{- $DB_PASSWORD := .Values.previewConfigmap.DB_PASSWORD }}
        - {{ .Values.cloudsqlproxy.databaseType }}
        - host=127.0.0.1 port={{ $DB_PORT }} user={{ $DB_USER }} password=$DB_PASSWORD  sslmode=disable dbname={{ $DB_NAME }}
        - up
  {{- end }}
  {{- if eq .Values.project.environment "production" }}
    {{- $DB_USER := .Values.productionConfigmap.DB_USER }}
    {{- $DB_NAME := .Values.productionConfigmap.DB_NAME }}
    {{- $DB_HOST := .Values.productionConfigmap.DB_HOST }}
    {{- $DB_PORT := .Values.productionConfigmap.DB_PORT }}
    {{- $DB_PASSWORD := .Values.productionConfigmap.DB_PASSWORD }}
        - {{ .Values.cloudsqlproxy.databaseType }}
        - host=127.0.0.1 port={{ $DB_PORT }} user={{ $DB_USER }} password=$DB_PASSWORD sslmode=disable dbname={{ $DB_NAME }}
        - up
  {{- end }}
{{- end }}
{{- end }}
